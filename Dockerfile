FROM golang:alpine3.15 AS builder

COPY message.go message.go
RUN go build -ldflags '-w -s -extldflags "-static"' -a -o app *.go

FROM scratch

COPY --from=builder /go/app /bin/app

ENTRYPOINT [ "app" ]